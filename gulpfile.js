var gulp = require('gulp'),
	sass = require('gulp-sass'),
	mmq = require('gulp-merge-media-queries'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	csscomb = require('gulp-csscomb'),
	watch = require('gulp-watch'),
	browserSync = require('browser-sync').create(),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	del = require('del'),
	runSequence = require('run-sequence'),
	sourcemaps = require('gulp-sourcemaps');

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: './'
    },
  });
});

gulp.task('sass', function() {
    var processors = [
        autoprefixer({browsers: ['last 4 version']})
    ];
    return gulp.src('scss/*.scss')
     	.pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sass({
        	style: 'nested',
        	sourcemap: true
        }).on('error', sass.logError))
		.pipe(postcss(processors))
	 	.pipe(mmq({
	      log: true
	    }))
	    .pipe(csscomb())
	    .pipe(sourcemaps.write(''))
        .pipe(gulp.dest('css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('images', function(){
  return gulp.src('sourceimages/**/*.+(png|jpg|gif|svg)')
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('images'))
	.pipe(browserSync.reload({
		stream: true
	}));
});

gulp.task('clean:css', function() {
  return del.sync('css');
});

gulp.task('watch', ['browserSync', 'sass', 'images'], function() {
	gulp.watch('scss/*.scss', ['sass']);
	gulp.watch('sourceimages/**/*.+(png|jpg|gif|svg)', ['images']);
	gulp.watch('*.html', browserSync.reload); 
	gulp.watch('js/**/*.js', browserSync.reload); 
});

gulp.task('default', function (callback) {
  runSequence('clean:css', 
    ['sass', 'browserSync', 'watch'],
    callback
  );
});
